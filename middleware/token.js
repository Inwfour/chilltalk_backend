var fs = require('fs')
var jwt = require('jsonwebtoken');

const tokenMiddleWare = async (req, res, next) => {
  console.log(req.headers.authorization);
  if (req.headers.authorization) {
    try {
        var token = req.headers.authorization.split(' ')
        var publicKey = fs.readFileSync('secret/public.key')
      jwt.verify(token[1], publicKey)
        next()
    } catch (error) {
      res.send({ message: 'Unauthorization' })
    }
  } else {
    res.send({ message: 'Token Not Found' })
  }
};

module.exports = tokenMiddleWare