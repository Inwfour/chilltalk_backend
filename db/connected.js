var mongoose = require('mongoose')

mongoose.connect('mongodb://localhost:27017/chilltalk_db', { useNewUrlParser: true }, (err) => {
  if (!err) console.log('connected db success')
  else console.log('err db')
})

module.exports = { mongoose }
