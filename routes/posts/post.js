var express = require('express')
var mongoose = require('mongoose')
var router = express.Router()
var Post = require('../../models/post')
var User = require('../../models/user')
var tokenMiddleWare = require('../../middleware/token')
const io = require('socket.io')

/*
! GET posts all.
*/
router.get('/posts', async (req, res, next) => {
  try {
    var post = await Post.find()
    post.sort((a,b) => b.updatedAt - a.updatedAt)
    res.send(post)
  } catch (error) {
    res.status(400).send({ message: 'Post not found' })
  }
})

/*
! GET post one.
*/
router.get('/posts/:id',tokenMiddleWare, async (req, res, next) => {
  try {
    var post = await Post.find({ userid: req.params.id })
    res.send(post)
  } catch (error) {
    res.status(400).send({ message: 'Post by user not found' })
  }
})

/*
! GET post one by user.
*/

router.get('/posts/user/:userid',tokenMiddleWare, async (req, res, next) => {
  try {
    var post = await Post.findOne({ userid: req.params.userid })
    res.send(post)
  } catch (error) {
    res.status(400).send({ message: 'Post by user not found' })
  }
})

/*
! POST post.
*/

router.post('/posts',tokenMiddleWare, async (req, res, next) => {
  try {
    var post = new Post({
      userid: req.body.userid,
      post: req.body.post,
      allLike: 0
    })
    await post.save()
    res.status(201).send({ message: "Post success" })
  } catch (error) {
    res.status(400).send({ message: error })
  }
})

/*
! UPDATE post.
*/

router.put('/posts/:id',tokenMiddleWare, async (req, res, next) => {
  try {
   var user = await User.findOne({ _id: req.body.userid })
   if (user._id == req.body.userid) {
      await Post.findByIdAndUpdate({ _id: req.params.id }, req.body)
      res.status(201).send({ message: "Update success" })
   } else {
     res.status(400).send({ message: "User Id Not Match" })
   }
  } catch (error) {
    res.status(400).send({ message: "Update Failed" })
  }
})

/*
! DELETE post.
*/

router.delete('/posts/:id',tokenMiddleWare, async (req, res, next) => {
  try {
      await Post.findOneAndDelete({ _id: req.params.id })
      res.status(201).send({ message: "Delete success" })
  } catch (error) {
    res.status(400).send({ message: "Delete Failed" })
  }
})

/*
! LIKE post.
*/

router.post('/posts/like',tokenMiddleWare, async (req, res, next) => {
  try {
    io.listen(5000)
    // io.origins('*:*')
    io.on('connection', client => {
      console.log('user connected')
    
      // เมื่อ Client ตัดการเชื่อมต่อ
      client.on('disconnect', () => {
          console.log('user disconnected')
      })
  
      // ส่งข้อมูลไปยัง Client ทุกตัวที่เขื่อมต่อแบบ Realtime
      client.on('sent-message', function (message) {
        console.log('bbb')
          io.sockets.emit('new-message', message)
      })
  })
    // var post = await Post.findById({ _id: req.body._id })
    // var yourlike = post.like.find(item => item.userid == req.body.userid)
    // if (yourlike == undefined) {
    //   post.like.push({
    //     userid: req.body.userid,
    //     status: true
    //   })
    // } else {
    //   if (yourlike.status === true) 
    //   { yourlike.status = false
    //   }
    //   else { yourlike.status = true
    //   }
    // }
    // await post.markModified('like')
    // await post.markModified('allLike')
    // await post.save()
    res.send({ message: 'Like Success' })
  } catch (error) {
    res.status(400).send({ message: "Delete Failed" })
  }
})

module.exports = router
