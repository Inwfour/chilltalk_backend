var express = require('express')
var router = express.Router()
var User = require('../../models/user')

/*
! SAVE users.
*/
router.post('/users', async (req, res, next) => {
  try {
    var { email,password, nickname, image } = req.body
    var splitImage = image.split('/')[5]
    var user = new User({
      email: email,
      image: `http://localhost:5000/images/${splitImage}`,
      password: password,
      nickname: nickname
    })
    if (await User.findOne({ email: user.email }) || await User.findOne({ nickname: user.nickname })) {
      res.status(400).send({ message: 'email or nickname invalid' })
    } else {
      await user.save()
      res.status(201).send({ message: 'Save Success' })
    }
  } catch (err) {
    res.status(400).send({ message : err })
  }
})

/*
! UPDATE users.
*/
router.put('/users/:id', async (req, res, next) => {
  try {
    var { email, nickname, image } = req.body
    var splitImage = image.split('/')[5]
    var user = ({
      email: email,
      image: `http://localhost:5000/images/${splitImage}`,
      nickname: nickname
    })
    if (await User.findOne({ email: user.email }) || await User.findOne({ nickname: user.nickname })) {
      await User.findByIdAndUpdate({ _id: req.params.id }, user)
      res.status(201).send({ message: 'Update Success' })
    } else {
      res.status(400).send({ message: 'email or nickname invalid' })
    }
  } catch (err) {
    res.status(400).send({ message : err })
  }
})

module.exports = router
