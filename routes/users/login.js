var express = require('express')
var router = express.Router()
var middlewere = require('../../middleware/auth')
var jwt = require('jsonwebtoken');
var fs = require('fs')

/*
! LOGIN users.
*/
router.post('/login', middlewere,  async (req, res, next) => {
  try {
    var privateKey = fs.readFileSync('secret/private.key')
    var token = jwt.sign({ payload: { email: req.body.email  } }, privateKey, { algorithm: 'RS256'}, { expiresIn: '1h' });
    res.send({
      user: req.user,
      token: token 
    })
  } catch (err) {
    res.status(400).send({ message : "Login failed" })
  }
})

module.exports = router
