var express = require('express')
var router = express.Router()
var User = require('../../models/user')
var tokenMiddleWare = require('../../middleware/token')

/*
! GET users all.
*/
router.get('/users', async (req, res, next) => {
  try {
    var user = await User.find()
    res.send(user)
  } catch (error) {
    res.status(400).send({ message: 'User not found' })
  }
})

/*
! GET user one.
*/
router.get('/users/:id', async (req, res, next) => {
  try {
    var user = await User.findById({ _id: req.params.id })
    res.send(user)
  } catch (error) {
    res.status(400).send({ message: 'User not found' })
  }
})

module.exports = router
