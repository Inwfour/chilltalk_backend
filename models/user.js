const mongoose = require('mongoose')
const Schema = mongoose.Schema
const isEmail = require('validator/lib/isEmail')

var User = new Schema(
  {
    email: { type: String, unique: true, required: true, validate: [isEmail, 'invalid email'] },
    image: { type: String, required: true },
    password: { type: String, required: true },
    nickname: { type: String, unique: true, required: true }
  },
  {
    timestamps: true
  },
  {strict: true}
)

module.exports = mongoose.model('users', User)
