var mongoose = require('mongoose')
var Schema = mongoose.Schema

var Post = new Schema({
  userid: String,
  post: String,
  like: { type: Array, default: [] },
},{
  timestamps: true
})

module.exports = mongoose.model('posts', Post)